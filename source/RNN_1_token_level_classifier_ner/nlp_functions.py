import numpy as np
import os
import spacy


def get_pad_vector(size):
    padding_vector = np.zeros((size,), dtype=np.float32)
    return padding_vector


def get_placeholder_vector(size):
    placeholder_vector = np.ones((size,), dtype=np.float32)
    return placeholder_vector


def spacy_parser(nlp, text):
    # https://spacy.io/api/doc
    doc = nlp(text)
    tokens = []
    vectors = []
    # https://spacy.io/usage/linguistic-features#sbd
    for token in doc:
        # https://spacy.io/api/token
        # https://spacy.io/usage/linguistic-features#lemmatization
        # https://spacy.io/api/lemmatizer
        # https://spacy.io/usage/linguistic-features#vectors-similarity

        # print(token.text, token.has_vector, token.vector_norm, token.is_oov)
        if token.has_vector:
            vector = token.vector
        else:
            vector = get_placeholder_vector(300) # ((300,1) is the shape of the vector in Spacy)

        tokens.append(token.text)
        vectors.append(vector)
    # print("tokens with no vectors:", no_vector)
    # if there is a very low count of vectors: https://spacy.io/usage/linguistic-features#adding-vectors 
    return tokens, vectors




### 
### 2. Word2Vec
### 
def get_word2vec_embeddings(nlp_resource, text):
    '''
    *nlp_resource* is the filename containing the vectors needed for loading.
    '''
    # https://www.shanelynn.ie/word-embeddings-in-python-with-spacy-and-gensim/
    # https://www.kaggle.com/pierremegret/gensim-word2vec-tutorial
    # https://radimrehurek.com/gensim/models/keyedvectors.html

    import gensim
    from gensim.models import KeyedVectors

    current_file_directory = os.path.dirname(os.path.abspath(__file__))
    os.chdir(current_file_directory)
    EMBEDDING_DIR = os.path.join(current_file_directory, 'saved_models')
    if not os.path.exits(EMBEDDING_DIR):
        os.makedirs(EMBEDDING_DIR)
        print("Error, the file is not in directory")
        return None

    else:
        # Word2Vec_embedding_file = "GoogleNews-vectors-negative300.bin"
        Word2Vec_embeddings = os.path.join(EMBEDDING_DIR, nlp_resource)
        print("Loading:", Word2Vec_embeddings)
        w2v_model = KeyedVectors.load_word2vec_format(Word2Vec_embeddings, binary=True)

    ### parsing ###
    embeddings = []
    added_embeddings = 0
    not_added = 0
    problem_tokens = []

    tokens = text.split(' ') # --> zdravoseljačka
    for t in tokens:
        try:
            # https://radimrehurek.com/gensim_3.8.3/models/keyedvectors.html#gensim.models.keyedvectors.Word2VecKeyedVectors
            embeddings.append(w2v_model.get_vector(t)) 
            added_embeddings += 1
        except KeyError:
            embeddings.append(get_placeholder_vector(300))
            not_added += 1
            problem_tokens.append(t)

    print(not_added, "tokens not added:", problem_tokens)
    print(added_embeddings, "added_tokens")

    return tokens, embeddings


### 
### 2. GloVe
### 
def get_glove_embeddings(nlp_resource, text):
    '''
    *nlp_resource* is the filename containing the vectors needed for loading
    '''

    # https://www.analyticsvidhya.com/blog/2020/03/pretrained-word-embeddings-nlp/
    # https://machinelearningmastery.com/use-word-embedding-layers-deep-learning-keras/

    current_file_directory = os.path.dirname(os.path.abspath(__file__))
    os.chdir(current_file_directory)
    EMBEDDING_DIR = os.path.join(current_file_directory, 'datasets')
    if not os.path.exits(EMBEDDING_DIR):
        os.makedirs(EMBEDDING_DIR)
        print("Error, the file {} is not in directory".format(nlp_resource))
        return None

    else:
        GloVe_embeddings = os.path.join(EMBEDDING_DIR, nlp_resource)
        print("Loading:", GloVe_embeddings)
    embeddings_index = {}
    with open(GloVe_embeddings, "r", encoding="utf-8") as file:
        for line in file:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype="float32")
            print(coefs.shape)
            vector_size = coefs.shape[0]
            embeddings_index[word.strip().lower()] = coefs
    print("Loaded %s word vectors." % len(embeddings_index))

    tokens = text.split(' ')

    embeddings = []
    added_embeddings = 0
    problem_tokens = []

    for token in tokens:
        t = token.strip().lower()
        if t in embeddings_index.keys():
            embeddings.append(embeddings_index[token])
            added_embeddings += 1
        else:
            embeddings.append(get_placeholder_vector(vector_size))

    print(len(problem_tokens), "tokens not added:", problem_tokens)
    print(added_embeddings, "added_tokens")

    return tokens, embeddings



### creating a dictionary from pretrained embeddings that is used to set up weights in the training model ###
# embeddings_dict = {}
# with open("pretrained_embeddings/glove.6B.300d.txt", 'r', encoding="utf-8") as f:
#     for line in f:
#         values = line.split()
#         word = values[0]
#         vector = np.asarray(values[1:], dtype="float32")
#         embeddings_dict[word] = vector

# embedding_matrix = np.zeros((vocab_size, 300))
# for word, i in t.word_index.items():
# 	embedding_vector = embeddings_dict.get(word)
# 	if embedding_vector is not None:
# 		embedding_matrix[i] = embedding_vector
#####################


### ### ### ### ### ### ### 
### Custom VECTORIZATION ### 
def vectorize():
    return True