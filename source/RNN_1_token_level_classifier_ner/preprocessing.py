
import os
# import re
import json
import csv
import string

import numpy as np
import pandas as pd



def read_food_json(filepath):
    with open(filepath) as f:
        data = json.loads(f.read())

    texts = []
    foods = []
    labels = []

    for conversation in data:
        utterances = conversation['utterances'] # --> utterances je lista
        for utterance in utterances: # --> svaki utterance je dict()
            text = utterance['text'] # --> u text spremamo sve stringove ('textove' iz izvornog filea)
            texts.append(text) # ok

            try:
                fs = []
                anns = []
                for segment in utterance['segments']: # utterance['segments'] je lista
                    food_words = segment['text'] # --> iz dictionaryja dobivamo opet 'text'
                    fs.append(food_words)

                    annotations = segment['annotations'] # --> tražimo oznaku koju su oni stavili za ovaj gornji 'food_words'
                    for annotation in annotations:
                        anns.append(annotation['name'])

                foods.append(fs)
                labels.append(anns)

            except KeyError: # try-except blok koristimo zato što ponekad ne postoji Key 'segments' u našem utterance dictu
                # iza ključne riječi except ubacujete onaj ERROR ili EXCEPTION koji očekujete
                # u slučaju dictionaryja (kad nema određenog key-a) to je KeyError
                foods.append([])
                labels.append([])
                continue # --> ovo govori našem try-except bloku da u slučaju KeyErrora samo nastavi dalje, prije toga smo spremili praznu listu (nema anotacija)

    # print(texts)
    assert len(texts) == len(foods) == len(labels) # --> assert služi tome da testirate neku svoju pretpostavku
        # --> ovdje samo testiramo da su nam sve liste (originalni tekstovi; koji tokeni su 'hrana'; spremljene anotacije) jednako dugačke

    i = 56
    print(texts[i])
    print(foods[i])
    print(labels[i])

    return texts, foods, labels







def encode_labels_from_tokens(df):
    '''
    ova funkcija gleda token po token matcha li nešto u 'food' columnu i po tome dodaje labele.

    Poboljšanja:
        --> ovdje gledamo je li token možda stopword i mičemo ga iz stupca foods i iz stupca tokens.
        --> bolji tokenizer?
        --> interpunkcije bolje riješiti
    Obavezno:
        - riješiti problem kod kojeg imamo dva ista tokena (od kojih jedan pripada grupi food labela)
    '''
    labels = []
    for idx,row in df.iterrows():
        row_labels = []
        for t in row['tokens']:
            no_punct_t = t.translate(str.maketrans('', '', string.punctuation)) # --> zamijenite s regexom
            if no_punct_t in row['foods']:
                row_labels.append(1) # One-hot: [0,1,0]
            else:
                row_labels.append(0) # One-hot: [1,0,0]

        labels.append(row_labels)

    df['labels'] = labels
    return labels



if __name__ == "__main__": # --> ovo služi samo da izvršimo stvari iz ovog filea (u slučaju da je ovaj file 'preprocesing.py' importan dalje)
    import datetime
    begin_time = datetime.datetime.now()
    # os.path.dirname(os.path.realpath(__file__)) # --> ovaj blok pita os modul pythona gdje se nalazi file koji trenutno runamo (cijeli path od tog filea)
    # os.getcwd() # --> bolji za jupyter notebooke; on gleda 'path' koji je trenutno u terminalu (terminal pokreće vašu python skriptu kad stisnete onaj play gore u vs codeu)
    data_path = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),'data')
    filename = os.path.join(data_path,'food-ordering.json')

    texts, foods, labels = read_food_json(filename)

    food_df = pd.DataFrame(
        {
            'texts': texts,
            'foods': foods,
            'annotations': labels
        })

    food_df['labels'] = food_df['annotations'].apply(lambda row: 0 if len(row)==0 else 1)
    # print(food_df['labels'].head(10))

    # https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_csv.html
    food_df.to_csv(os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),'data','RNN_1_data.csv'))

    print(datetime.datetime.now() - begin_time)


    