import os
import time
import math
import numpy as np
import tensorflow as tf
# from preprocessing import get_pad_vector
padding_vector = np.zeros((300,), dtype=np.float32)


def load_tf_models(model_name):
    current_file_directory = os.path.dirname(os.path.abspath(__file__))
    models_directory = os.path.dirname(current_file_directory)

    model_path = os.path.join(models_directory, "models", model_name)
    model = tf.keras.models.load_model(model_path)
    return model


def int_decoding(encoded_data):
    if not isinstance(encoded_data, list):
        encoded_data = encoded_data.tolist()

    labels = []
    for l in encoded_data[0]:
        if l == 0:
            label = "NOT FOOD"
        elif l == 1:
            label = "FOOD"
        elif l == 2:
            label = "PADDING"
        labels.append(label)

    return labels



if __name__ == '__main__':
    import spacy
    from nlp_functions import spacy_parser
    nlp = spacy.load("en_core_web_lg")


    model_name = "rnn1_token_2"
    max_len = 32 # what shape did you give as an input for your model ? 

    model = load_tf_models(model_name)

    start = time.time()

    ### WORD PREDICTIONS
    # word = "snack"
    # _, word_vector = spacy_parser(nlp, word)
    # print("word_vector:", word_vector.shape)
    # pred = model.predict(word_vector, verbose=1)
    # print(pred)

    ### SEQUENCE PREDICTIONS
    text = "I like to eat snacks and stuff, but quinoa is also ok, I guess..."
    
    tokens,vectors = spacy_parser(nlp,text)
    if len(vectors) > max_len:
        vectors = vectors[:max_len]
    else:
        while len(vectors) < max_len:
            vectors.append(padding_vector)
    
    vec_text = np.array(vectors).reshape(-1, max_len, len(padding_vector))

    class_predictions = model.predict(vec_text, verbose=1)
    print(class_predictions.shape) # --> model broj input tekstova * n time-stepa (tokeni) * 3 output predictiona (softmax vjerojatnosti za svaku od 3 klasa koje smo imali: food, not_food, padding)
    


    ### Kako dobiti max vrijednost od 3 klase za svaki token? 
    classes = class_predictions.argmax(axis=2)
    labels = int_decoding(classes)

    predictions = dict()
    for idx,token in enumerate(tokens[:max_len]):
        predictions[token] = labels[idx]

    print(predictions)

    end = time.time()
    print("Time for predictions:", end - start)


