

import os
import pandas as pd
import numpy as np
import spacy
import string


### LOADING DATA
datapath = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'data')
food_df = pd.read_csv(os.path.join(datapath, 'data.csv'))
# print(food_df.sample(5))


### VEKTORIZACIJA TOKENA:
from nlp_functions import spacy_parser
nlp = spacy.load("en_core_web_lg")

tokens = []
vectors = []
for idx,row in food_df.iterrows():
    t,v = spacy_parser(nlp,row['texts'])
    tokens.append(t)
    vectors.append(v)

food_df['tokens'] = tokens


### kodiranje labela - po tokenu
from preprocessing import encode_labels_from_tokens

labels = encode_labels_from_tokens(food_df)
# print(labels)


### TESTING THE TOKENS AND LABELS (lengths)
i = 56
print(tokens[i])
# print(vectors[56])
print(labels[i])


import random
for x in range(10):
    i = random.randint(0,len(tokens)-1)
    assert len(tokens[i]) == len(labels[i])


### provjera shapeova u napravljenim listama
for i,text in enumerate(vectors):
    assert len(vectors[i]) == len(labels[i]) == len(tokens[i])



### GETTING MAX_LEN of SEQUENCES
food_df['TokenCount'] = food_df['tokens'].str.len()
max_len = food_df['TokenCount'].max()
print("max_len:", max_len)
max_len = 8



### PADDING ###
embeddings = []
enc_labels = []

for i,text in enumerate(vectors):
    seq = []
    l = []

    if len(text) > max_len:
        seq = text[:max_len]
        l = labels[i][:max_len]
    else:
        seq.extend(text)
        l.extend(labels[i])
        while len(seq) < max_len:
            seq.append(np.zeros((300,), dtype=np.float32))
            l.append(2) # One-hot: [0,0,1]

    embeddings.append(seq)
    enc_labels.append(l)

# print("rečenica 128:", len(embeddings[128]))
# print("dužina vektora za prvi token u rečenici 1111:", len(embeddings[1111][0]))
# print(enc_labels[1111][0])
# print(enc_labels[1111])
# print(food_df['texts'][1111])



##############################################################################




    ### smanjujem si input da se model istrenira brže
test_embeddings = embeddings #[10000:]
test_labels = enc_labels #[10000:]


### CONVERTING INPUT DATA TO ARRAYS (OR TENSORS)
embeddings = np.asarray(test_embeddings).astype('float32')
print(embeddings.shape)

#.reshape(len(enc_labels),max_len)
# enc_labels = np.array(enc_labels, dtype="float32") # , dtype=object
enc_labels = np.array(test_labels).astype(np.float32)

# from tensorflow.keras.utils import to_categorical
# enc_labels = to_categorical(enc_labels, num_classes=3)

# print(enc_labels)




# ### SPLITTING DATA INTO TRAIN and TEST ###
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(
    embeddings, enc_labels, test_size=0.2, random_state=42
)

print("X_train.shape:", X_train.shape)
print("y_train.shape:", y_train.shape)




# ############# T R A I N I N G #############
model_name = 'lipik-nlp-1'
model_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "models")

import tensorflow as tf


embedding_dim = X_train.shape[2]
input_shape = (X_train.shape[1], X_train.shape[2])

print("embedding_dim:", embedding_dim)
print("input_shape:", input_shape)

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Embedding, Input, Dense, LSTM, SpatialDropout1D, Dropout, TimeDistributed
from tensorflow.keras.callbacks import EarlyStopping

model = Sequential()
# model.add(Embedding(MAX_NB_WORDS, EMBEDDING_DIM, input_length=X.shape[1]))
    # ne treba nam Embedding layer jer MI NJEMU DAJEMO embeddinge :) 
model.add(Input(shape=input_shape))
    # https://keras.io/api/layers/core_layers/input/
    # koristi se da instancira "Input Tensore", tj pretvara naše input vektore sebi u Tensore
    # ovo je mjesto gdje na outputu nastaje error ako Tensorflow i Numpy nisu kompatibilni
model.add(LSTM(16, return_sequences=True)) # dropout=0.2, recurrent_dropout=0.2
# model.add(Dense(max_len, activation='softmax'))
model.add(TimeDistributed(Dense(3, activation="softmax")))

model.compile(loss='sparse_categorical_crossentropy', optimizer= 'Nadam' , metrics=['accuracy'])

# for layer in model.layers:
#     layer.summary()

print(model.output_shape)
print(model.compute_output_signature)

print(model.summary())
print("-------------------")




epochs = 1
batch_size = 8

history = model.fit(
    X_train, y_train, 
    epochs=epochs, batch_size=batch_size, 
    validation_split=0.1, 
    callbacks=[EarlyStopping(monitor='val_loss', patience=2, restore_best_weights=True)]
    )


### VISUALIZING TRAINING and TESTING ###
# import matplotlib.pyplot as plt

# plt.title('Loss')
# plt.plot(history.history['loss'], label='train')
# plt.plot(history.history['val_loss'], label='test')
# plt.legend()
# plt.show()


accr = model.evaluate(X_test,y_test)
print('Test set\n  Loss: {:0.3f}\n  Accuracy: {:0.3f}'.format(accr[0],accr[1]))


# plt.title('Accuracy')
# plt.plot(history.history['accuracy'], label='train')
# plt.plot(history.history['val_accuracy'], label='test')
# plt.legend()
# plt.show()



# ### SAVING THE MODEL

try:
    # nekad budu errori zbog tensorflowa :) kad se koristi dropout
    model.save(os.path.join(model_path, model_name), save_format="tf")

except:
    model.save(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "saved_models", (model_name+'.h5')))
    print("model {} saved as .h5 keras format".format(model_name))
    # return '''model.save() is possible only with tensorflow==2.2.0 
    #         OR when training a model with dropout and rec_droput set values to 0.
    #         Check https://stackoverflow.com/questions/58720387/unable-to-save-tensorflow-keras-lstm-model-to-savedmodel-format?rq=1 for more info'''

# model.save(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'saved_models', model_name), save_format="h5")


