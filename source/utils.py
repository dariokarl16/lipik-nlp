
import os
import pandas as pd


### Load the file into dataframe 
def get_df(filename:str):
    data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')
    filepath = os.path.join(data_path, filename)

    if filepath.endswith('.pickle'):
        df = pd.read_pickle(filepath)
    elif filepath.endswith('.csv'):
        df = pd.read_csv(filepath)
    else:
        print(f"cannot open file {filepath}, returning an empty dataframe object")
        df = pd.DataFrame()

    return df, filepath
