

import uvicorn
from fastapi import FastAPI, Header
from typing import Optional
from fastapi.params import Path
# from pydantic import BaseModel


import os
# import argparse
# import json
# import requests
# import datetime
import numpy as np
import tensorflow as tf
import spacy


### IMPORTING PROJECT FUNCTIONS
from token_level_classifier_ner.nlp_functions import spacy_parser, get_pad_vector
from token_level_classifier_ner.testing import load_tf_models, int_decoding


### LOADING THE APP resources
nlp = spacy.load("en_core_web_lg")
max_len = 32
padding_vector = get_pad_vector(300)
model_name = "lipik-nlp-5"
model = load_tf_models(model_name)


# Define application
app = FastAPI(
    title="food classifier",
    description="for an input text, returns which tokens are considered to be 'food-based'",
    version="0.1",
)


### ROUTES ###
from http import HTTPStatus
@app.get("/status")
def _index():
    """Health check."""
    # response = {
    #     "response_status": {
    #         "message": HTTPStatus.OK.phrase,
    #         "status-code": HTTPStatus.OK,
    #     },
    #     "responses_data": {
    #         "Model List call": "",
    #         "Individual model call": "",
    #         "Call model by name": ""
    #     },
    # }
    response = "bok"
    return response


@app.post("/predict")
def predict_classes(text: str):

    response = dict()

    response["text"] = text

    tokens,vectors = spacy_parser(nlp,text)
    if len(vectors) > max_len:
        vectors = vectors[:max_len]
    else:
        while len(vectors) < max_len:
            vectors.append(padding_vector)

    vec_text = np.array(vectors).reshape(-1, max_len, len(padding_vector))
    class_predictions = model.predict(vec_text)
    classes = class_predictions.argmax(axis=2)

    labels = int_decoding(classes)

    predictions = dict()
    for idx,token in enumerate(tokens[:max_len]):
        predictions[token] = labels[idx]

    response["predictions"] = predictions
    
    return response


if __name__ == '__main__':
    uvicorn.run(app, host='127.0.0.1', port=8000)

    