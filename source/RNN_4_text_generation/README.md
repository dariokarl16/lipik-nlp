ToDo:
1. Load the restaurants dataset (again)
- This time, you only need the texts (messages in and out, whatever you can get you hands on)
- Use dataset from task 1 for training and testing (because we have pairs of question->answer here...)
- Use dataset from task 2 for inferencing (we have only "user" inputs)
2. The preprocessing part will be a lot easier
- You can do some cleaning and adjustments, but not too much
- We want our model to learn from natural language and to generate the next best possible token and/or character
3. This model, you will want to deploy as a web app and test is as much as possible
- How would you track the requests to your service and the responses your model has given?
- How would you ask a user via the web app if he is happy with the response?
- Build a logging system or a small orm data model to save the data somewhere (txt,csv,sqlite?)


----------------------------------

4. Train a new text generation model, but use chapters of several books of your choice (choose a specific topic/domain of texts)
5. Does it work better than a "chatbot" application?
