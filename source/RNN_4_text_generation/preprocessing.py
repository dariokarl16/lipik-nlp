


import os
# import re
import json
import csv
import string

import numpy as np
import pandas as pd



def get_conv_pairs(filepath):
    with open(filepath) as f:
        data = json.loads(f.read())

    convIds = []
    user_messages = []
    assistant_messages = []
    idx = 0

    for conversation in data:
        conv_id = conversation["conversation_id"]
        utterances = conversation['utterances'] # --> utterances je lista
        for utterance in utterances: # --> svaki utterance je dict()
            if "text" in utterance.keys():
                text = utterance['text']

                if utterance['speaker'] == "ASSISTANT":
                    # this message is a reply to previous message --> UPDATE
                    if idx != 0 and conv_id == convIds[idx-1] and user_messages[idx-1] is not None:
                        print(f"replying to previous user message: {user_messages[idx-1]} with assistant message: {text}")

                        assistant_messages[idx-1] = text
                    
                    # additional assistent message to ITS previous
                    elif idx != 0 and conv_id == convIds[idx-1] and user_messages[idx-1] is None and assistant_messages[idx-1] is not None:
                        previous_message = assistant_messages[idx-1]
                        new_message = previous_message + " " + text
                        assistant_messages[idx-1] = new_message

                    else:
                        # this is a new message from assistant
                        assistant_messages.append(text)
                        user_messages.append(None)
                        convIds.append(conv_id)
                        idx += 1


                elif utterance['speaker'] == "USER":
                    # if the user is replying to his own message (adding text)
                    if idx != 0 and conv_id == convIds[idx-1] and user_messages[idx-1] is not None and assistant_messages[idx-1] is None:
                        previous_message = user_messages[idx-1]
                        new_message = previous_message + " " + text
                        user_messages[idx-1] = new_message

                    else:
                        # if this is a new message (after assistant's)
                        user_messages.append(text)
                        assistant_messages.append(None)
                        convIds.append(conv_id)
                        idx += 1

                else:
                    print(utterance['speaker'])
            
            else:
                pass


    assert idx == len(convIds) == len(user_messages) == len(assistant_messages)
    i = 56
    print(convIds[i])
    print(user_messages[i])
    print(assistant_messages[i])

    return convIds,user_messages,assistant_messages



### Load the file into dataframe 
def get_df(filename:str):
    data_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'data')
    filepath = os.path.join(data_path, filename)

    if filepath.endswith('.pickle'):
        df = pd.read_pickle(filepath)
    elif filepath.endswith('.csv'):
        df = pd.read_csv(filepath)
    else:
        print(f"cannot open file {filepath}, returning an empty dataframe object")
        df = pd.DataFrame()

    return df, filepath


def clean_text(text):
    '''
    '''
    ### punctuation:
    # txt = "".join(t for t in text if t not in string.punctuation).lower()
    txt = str(text).lower().strip()
    txt = txt.encode("utf8").decode("ascii",'ignore')
    return txt


def concat_conversation():
    pass


def concat_responses(question:str, answer:str):
    concat = ""
    if str(answer) == 'nan':
        answer = ""
    concat = str(question).strip() + " " + str(answer).strip()

    return concat



if __name__ == "__main__":
    import datetime
    begin_time = datetime.datetime.now()
    # os.path.dirname(os.path.realpath(__file__)) # --> ovaj blok pita os modul pythona gdje se nalazi file koji trenutno runamo (cijeli path od tog filea)
    # os.getcwd() # --> bolji za jupyter notebooke; on gleda 'path' koji je trenutno u terminalu (terminal pokreće vašu python skriptu kad stisnete onaj play gore u vs codeu)
    data_path = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),'data')
    filename = os.path.join(data_path,'food-ordering.json')

    rows, user_msg, assistant_msg = get_conv_pairs(filename)

    df = pd.DataFrame(
        {
            'ConvId': rows,
            'UserMessages': user_msg,
            'AssResponses': assistant_msg
        })

    df.to_csv(os.path.join(data_path,'RNN_4_conversations.csv'))

    print(datetime.datetime.now() - begin_time)


    