

from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.utils import to_categorical # Converts a class vector (integers) to binary class matrix.


import numpy as np


def create_ngram_sequences(corpus, tokenizer_object=Tokenizer()):
    '''
        This function is used to tokenize all the sequences and convert them to ngram sequences.
        It takes a sequence and creates varying length n-grams up to the count of tokens in a sequence
    '''
    ## tokenization
    tokenizer_object.fit_on_texts(corpus)
    total_words = len(tokenizer_object.word_index) + 1
    
    ## convert data to a token sequence 
    input_sequences = []
    for line in corpus:
        token_list = tokenizer_object.texts_to_sequences([line])[0]
        for i in range(1, len(token_list)):
            n_gram_sequence = token_list[:i+1]
            input_sequences.append(n_gram_sequence)
    return input_sequences, total_words, tokenizer_object



def get_optimal_max_len():
    '''
    '''
    pass


def generate_padded_sequences(input_sequences, word_count:int):
    '''
        In this function we:
            1. decide the max len of sequences (explore the data to decide which one is the best)
            2. pad the sequences with a padding token
    '''
    max_sequence_len = max([len(x) for x in input_sequences]) # this is okay only for the first version...
    input_sequences = np.array(pad_sequences(input_sequences, maxlen = max_sequence_len - 1, padding='pre'))
    
    # this is a point where we:
        # 1. get the tokens as training inputs
        # 2. get the next word that our model needs to predict based on 1.
    # print(input_sequences.shape)

    predictors, label = input_sequences[:,:-1], input_sequences[:,-1]
    label = to_categorical(label, num_classes = word_count)

    return predictors, label, max_sequence_len

