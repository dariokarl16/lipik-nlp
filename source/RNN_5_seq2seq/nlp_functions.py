
import os
import numpy as np
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences



def create_vocab(text_lists, tokenizer_object=Tokenizer()):

    tokenizer_object = Tokenizer()
    tokenizer_object.fit_on_texts(text_lists)
    VOCAB_SIZE = len(tokenizer_object.word_index) + 1
    dictionary = tokenizer_object.word_index
    
    word2idx = {}
    idx2word = {}
    for k, v in dictionary.items():
        if v < VOCAB_SIZE:
            word2idx[k] = v
            idx2word[v] = k
        if v >= VOCAB_SIZE-1:
            continue
            
    return tokenizer_object, word2idx, idx2word, VOCAB_SIZE



def text2seq(encoder_text, decoder_text, tokenizer_object):

    encoder_sequences = tokenizer_object.texts_to_sequences(encoder_text)
    decoder_sequences = tokenizer_object.texts_to_sequences(decoder_text)
    
    return encoder_sequences, decoder_sequences



def padding(sequences, MAX_LEN):
  
    padded_sequence = pad_sequences(sequences, maxlen=MAX_LEN, dtype='int32', padding='post', truncating='post')
    
    return padded_sequence



def glove_100d_dictionary(GLOVE_DIR, ndims:int = 100):
    embeddings_index = {}
    with open(os.path.join(GLOVE_DIR, f"glove.6B.{ndims}d.txt")) as f:
        lines = f.readlines()
    for line in lines:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs

    return embeddings_index


# def embedding_matrix_creater(embedding_dimention):
#     '''
#         Creates an embedding matrix from a vocabulary
#     '''
#     embedding_matrix = np.zeros((len(word_index) + 1, embedding_dimention))
#     for word, i in word_index.items():
#         embedding_vector = embeddings_index.get(word)
#         if embedding_vector is not None:
#             # words not found in embedding index will be all-zeros.
#             embedding_matrix[i] = embedding_vector
#     return embedding_matrix

def embedding_layer_creater(VOCAB_SIZE, EMBEDDING_DIM, MAX_LEN, embedding_matrix):
    '''
        
    '''
    embedding_layer = Embedding(input_dim = VOCAB_SIZE, 
                                output_dim = EMBEDDING_DIM,
                                input_length = MAX_LEN,
                                weights = [embedding_matrix],
                                trainable = False)
    return embedding_layer