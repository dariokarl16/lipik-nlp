
import os
import re
import pandas as pd
import numpy as np



def remove_unpaired_convs(df):
    '''
        The function removes the rows where we don't have user messages ("an input for the model") 
        and conversations where the user has finished the conversation.
    '''
    print(df.shape)

    new_df = df[df['UserMessages'].notna()]
    print(new_df.shape)

    # df[df['AssResponses'].isna()]
    df = new_df[new_df['AssResponses'].notna()]
    print(df.shape)

    return df



def clean_text(text):
    '''
    '''
    ### punctuation:
    # txt = "".join(t for t in text if t not in string.punctuation).lower()
    text = str(text).lower().strip()
    text = text.encode("utf8").decode("ascii",'ignore')

    text = re.sub(r"i'm", "i am", text)
    text = re.sub(r"he's", "he is", text)
    text = re.sub(r"she's", "she is", text)
    text = re.sub(r"it's", "it is", text)
    text = re.sub(r"that's", "that is", text)
    text = re.sub(r"what's", "that is", text)
    text = re.sub(r"where's", "where is", text)
    text = re.sub(r"how's", "how is", text)
    text = re.sub(r"\'ll", " will", text)
    text = re.sub(r"\'ve", " have", text)
    text = re.sub(r"\'re", " are", text)
    text = re.sub(r"\'d", " would", text)
    text = re.sub(r"\'re", " are", text)
    text = re.sub(r"won't", "will not", text)
    text = re.sub(r"can't", "cannot", text)
    text = re.sub(r"n't", " not", text)
    text = re.sub(r"n'", "ng", text)
    text = re.sub(r"'bout", "about", text)
    text = re.sub(r"'til", "until", text)
    text = re.sub(r"[-()\"#/@;:<>{}`+=~|.!?,]", "", text)
    
    return text



def seq_spec_tagger(decoder_input_sentence:str) -> str:
    '''
        This function 
    '''
    bos = "<BOS>"
    eos = "<EOS>"
    # final_target = [bos + text + eos for text in decoder_input_sentence]
    spec_decoder_input_sentence = bos + " " + decoder_input_sentence + " " + eos
    return spec_decoder_input_sentence



