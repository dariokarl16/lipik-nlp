

# Intent classifier
- recognize "What the customer is talking about"
- repo example: https://www.mindmeld.com/docs/blueprints/food_ordering.html 



### Download the data from:
1. https://github.com/amazon-research/food-ordering-semantic-parsing-dataset/tree/main/data
2. https://www.kaggle.com/datasets/swapnilpote/restaurant-chatbot-dataset-intent-entity?resource=download
3. Add other intents from: 
    3.1. https://github.com/clinc/oos-eval
    3.2. https://www.kaggle.com/datasets/hassanamin/atis-airlinetravelinformationsystem



### How-to
- parse the data from individual datasets
- get individual sentences/paragraphs with associated intent annotations
- reduce the size of dataset to max 10 000 sequences and number of intents to 5,6 (including a Out-of-scope **OOS** intent -> fillers)
    - see https://raw.githubusercontent.com/clinc/oos-eval/master/data/data_small.json OOS
- build a table with the following format:
| Sentence/Paragraph/Text | Intent (label/tag) |
| ----------------------- | ------------------ |
| "i would like a regular latte cinnamon iced with one extra espresso shot" | Food Order |



### The goal
- we want to use this model to recognize what are customers asking for
- maybe this will be implemented in their mailing system and they will check only those that are classified as food orders and use it their chatbot to parse the text if it's a food order (and do something else with another intent)
