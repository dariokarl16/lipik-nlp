
import os
import pandas as pd
import numpy as np

from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from sklearn.model_selection import train_test_split
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.layers import Dropout


model_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "models")
model_name = 'rnn2_sequence_class_3'


### LOAD THE DATA
datapath = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'data')
filename = "RNN_2_processed_intents_data.pickle"
data = pd.read_pickle(os.path.join(datapath, filename))


### PREPARE THE INPUT 
MAX_NB_WORDS = 10000 # this is the value for num_words parameter in tokenizer 
    # --> your task is to find out what is the vocabulary size of our dataset 

MAX_SEQUENCE_LENGTH = 16 
EMBEDDING_DIM = 100 # --> if you are using a pretrained model, get the length of a vector

tokenizer = Tokenizer(num_words=None, filters='!"#$%&()*+,-./:;<=>?@[\]^_`{|}~', lower=True)
tokenizer.fit_on_texts(data.clean_text.values)

word_index = tokenizer.word_index
print('Found %s unique tokens.' % len(word_index))

### set the num_word param:
tokenizer.num_words = len(tokenizer.word_index) + 1
MAX_NB_WORDS = len(tokenizer.word_index) + 1

### create X and Y and pad the sequences
X = tokenizer.texts_to_sequences(data.clean_text.values)
X = pad_sequences(X, maxlen=MAX_SEQUENCE_LENGTH)
print('Shape of data tensor:', X.shape)

Y = pd.get_dummies(data.intent).values 
    # --> see https://www.mygreatlearning.com/blog/label-encoding-in-python/
    # --> https://machinelearningmastery.com/how-to-prepare-categorical-data-for-deep-learning-in-python/
print('Shape of label tensor:', Y.shape)

### split dataset
X_train, X_test, Y_train, Y_test = train_test_split(X,Y, test_size = 0.20, random_state = 42)
print(X_train.shape,X_test.shape)
print(X_test.shape,Y_test.shape)


### pass it to a model
model = Sequential()
model.add(Embedding(MAX_NB_WORDS, EMBEDDING_DIM, input_length=X.shape[1]))
# model.add(SpatialDropout1D(0.2))
model.add(LSTM(32, dropout=0.2, recurrent_dropout=0.2))
model.add(Dense(Y.shape[1], activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='Nadam' , metrics=['accuracy'])
print(model.summary())


epochs = 10
# https://machinelearningmastery.com/how-to-control-the-speed-and-stability-of-training-neural-networks-with-gradient-descent-batch-size/#:~:text=Batch%20size%20controls%20the%20accuracy,stability%20of%20the%20learning%20process.
batch_size = 64

history = model.fit(
    X_train, Y_train, 
    epochs=epochs, batch_size=batch_size, 
    validation_split=0.1, 
    callbacks=[
        EarlyStopping(monitor='val_loss', patience=3, restore_best_weights=True) # model će se prestati trenirati ako mu se loss ne smanjuje 3 epohe za redom...
    ]
)


### testing metrics
import matplotlib.pyplot as plt

plt.title('Loss')
plt.plot(history.history['loss'], label='train')
plt.plot(history.history['val_loss'], label='test')
plt.legend()
plt.show();


accr = model.evaluate(X_test,Y_test)
print('Test set\n  Loss: {:0.3f}\n  Accuracy: {:0.3f}'.format(accr[0],accr[1]))


### save the model
try:
    # nekad budu errori zbog tensorflowa :) kad se koristi dropout
    model.save(os.path.join(model_path, model_name), save_format="tf")

except:
    model.save(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "saved_models", (model_name+'.h5')))
    print("model {} saved as .h5 keras format".format(model_name))

    
### TASK:
    # improve the preprocessing part
    # use a pretrained word2vec model 
    # explore BILSTM and/or GRU layers
    # change the optimizer and output dimension of the (BI)LSTM layer
    # increase the number of epochs and batch size

    # plot the test results into a confusion matrix and roc-auc curve 
        # what classes are being confused with by the model?


        