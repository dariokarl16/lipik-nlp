
import os
# import re
import json
import csv
from string import punctuation
import pandas as pd
import spacy
nlp = spacy.load("en_core_web_lg")
from spacy.lang.en import stop_words


def read_food_intent_json(filepath):
    with open(filepath) as f:
        data = json.loads(f.read())

    texts = list()
    intents = list()
    entities = list()
    for utterance in data["rasa_nlu_data"]["common_examples"]:
        text = utterance["text"]
        intent = utterance["intent"]
        _entities = utterance["entities"] # dont process here

        texts.append(text)
        intents.append(intent)
        entities.append(_entities)

    return texts, intents, entities


def read_other_intents_json(filepath):
    with open(filepath) as f:
        data = json.loads(f.read())

    texts = list()
    intents = list()

    for key in data.keys():
        for v in data[key]:
            t = v[0]
            i = v[1]
            texts.append(t)
            intents.append(i)

    return texts, intents


def clean_text(text):

    # lowercase text
    text = text.lower()

    # removing only punctuations
    punctuations = list(punctuation)
    text = [ word for word in text.split(' ') if word not in punctuations]

    # # convert to spacy object
    # s_text = nlp(text)

    # # lemmatizing
    # s_text = [ word.lemma_.strip() for word in sentence ]

    # # removing stop words and punctuations
    # sentence = [ word for word in sentence if word not in stop_words and word not in punctuations ]
    return text


def get_max_length(series_object, n=10):
    print(type(series_object.value_counts()))
    print(series_object.value_counts().sort_values(ascending=False).head(20))


def process_text(df):
    df['clean_text'] = df['text'].apply(lambda row: clean_text(row))
    df['TokenCount'] = df['clean_text'].str.len()

    ### find out where to truncate the texts
    max_len = get_max_length(df['TokenCount'])

    ### What is the distribution of our classes?
        # is it disbalanced ??
        
    return df





if __name__ == "__main__": # --> ovo služi samo da izvršimo stvari iz ovog filea (u slučaju da je ovaj file 'preprocesing.py' importan dalje)
    import datetime
    begin_time = datetime.datetime.now()
    # os.path.dirname(os.path.realpath(__file__)) # --> ovaj blok pita os modul pythona gdje se nalazi file koji trenutno runamo (cijeli path od tog filea)
    # os.getcwd() # --> bolji za jupyter notebooke; on gleda 'path' koji je trenutno u terminalu (terminal pokreće vašu python skriptu kad stisnete onaj play gore u vs codeu)
    data_path = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),'data')
    filename = os.path.join(data_path,'restaurant_intent_entities.json')
    filename2 = os.path.join(data_path,'other_intents_full.json')

    food_texts, food_intents, food_entities = read_food_intent_json(filename)
    df = pd.DataFrame(
        {
            'text': food_texts,
            'intent': food_intents
        }
    )
    other_texts, other_intents = read_other_intents_json(filename2)
    df = df._append(pd.DataFrame({'text': other_texts, 'intent': other_intents})) # or you can use concat or iterate list and use loc ...

    df = process_text(df)

    print(df.describe())

    df.to_pickle(os.path.join(data_path, 'RNN_2_processed_intents_data.pickle'))

    ### create a separate file with entities:
    df = pd.DataFrame(
        {
            'text': food_texts, # we are taking uprocessed text here (look at the structure of entities list...)
            'intent': food_intents,
            'entities': food_entities
        }
    )
    df.to_pickle(os.path.join(data_path, 'RNN_3_processed_restaurant_data.pickle'))

    print(datetime.datetime.now() - begin_time)
    