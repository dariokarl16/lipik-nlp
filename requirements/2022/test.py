

# 0. check your python version
    # echo .python-version
    # if not using conda:
        # https://realpython.com/intro-to-pyenv/
        # https://github.com/pyenv/pyenv # <--- highly recommended
# 1. create an environment
    # a) https://docs.python.org/3/library/venv.html
    # b) https://virtualenv.pypa.io/en/latest/ 
    # c) https://stackoverflow.com/a/68164027 
    # d) https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/
# 2. Install packages
    # run pip install -r requirements.txt 


import os
import re
import sys
import importlib.util
import importlib

print(f"python version: '{sys.version}'")
# assert str(sys.version).startswith('3.9.7')


def import_requirements_packages():
    project_root = os.path.dirname(os.path.abspath(__file__))
    requirements = list()
    with open(os.path.join(project_root, 'requirements.txt')) as f:
        requirements = f.readlines()

    for req in requirements:
        print("requirement:", req.strip())
        i = req.find('=')
        if i > 0:
            m = req[:i] 
            spec = importlib.util.find_spec(m)
            if spec is None:
                print("not found:", m)
            else:
                try:
                    module = importlib.import_module(m)
                except:
                    module = None
                finally:
                    print("module:", module, "\n")
                    # return module




from modulefinder import ModuleFinder

def find_modules():
    finder = ModuleFinder()
    finder.run_script('t_modules.py')

    print('Loaded modules:')
    for name, mod in finder.modules.items():
        print('%s: ' % name, end='')
        print(','.join(list(mod.globalnames.keys())[:3]))

    print('-'*50)
    print('Modules not imported:')
    print('\n'.join(finder.badmodules.keys()))



def zdravoseljacka():
    '''
    :D
    '''
    try:
        import spacy
        import gensim
        import fastapi
        from tensorflow.keras.models import Sequential
        from matplotlib.pyplot import plot_date
        import pydantic
        import fasttext
        
    except:
        print("NOT FOUND NEEDED MODULES")



if __name__ == "__main__":

    import_requirements_packages()

    zdravoseljacka()


