
# ABOUT
- This is an educational project repository used for NLP lectures @ AI Center in Lipik, Croatia
- There are several educational resources, presentations, "cheat-sheets" and exlanatory descriptions of concepts in AI, NLP, Python etc.
    - some of those resources have been created by the repo owner
    - several resources have been created by other authors with licences to distribute freely
        - links, references and names are passed within the documents
    - Please be advised to share any credits and references if distributing this content further as they can be treated as intellectual property.
- 

# Project Structure
- ...

# USAGE
- requirements.txt is a list of used packages and versions
- python 3.9.7 has been used for development

# CONTRIBUTION
- ...

# LICENCE
- see [**license.html**](license.html)
- https://creativecommons.org/licenses/by-nc-sa/4.0/

---------------

# OLD

### Dataset
- food dataset (food ordering and restaurants)
- check demo.ipynb for more info

### Training
- steps explained in the notebook
- train your model (clean the dataset first!)
- save the model

### Loading the model and exposing predictions
- install uvicorn with "conda install -c conda-forge uvicorn"
- choose the model to be loaded and assign the name to "model_name" variable
- run "python -m uvicorn app:app --reload" in the terminal (environment activated)
- go to http://127.0.0.1:8000/docs#/default/predict_classes_predict_post 
- "Try out" the /predict endpoint
- Insert input in the *"text"* area
- --> Execute and wait for the response
- FastAPI docs --> https://fastapi.tiangolo.com/tutorial/first-steps/
